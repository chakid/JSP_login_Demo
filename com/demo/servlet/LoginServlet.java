package com.demo.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 登录处理
 * @author Administrator
 */
@WebServlet(name="LoginServlet",urlPatterns={"/servlet/LoginServlet"})
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
	        throws ServletException, IOException {
		System.out.println("LoginServlet.doPost...");
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		
		if ("admin".equals(username) && "admin".equals(password)) {
			HttpSession session = req.getSession();
			session.setAttribute("username", username);
			String nickname = req.getParameter("nickname");
			if (nickname == null || nickname.length() == 0)
				nickname = "匿名用户";
			session.setAttribute("nickname", nickname);
			resp.sendRedirect(req.getContextPath()+"/login.sucess.jsp");
		} else
			resp.sendRedirect(req.getContextPath()+"/login.fail.jsp");
	}
}
